const controller = require('../controllers/indexController');

module.exports = (app, middleware = []) => {
    app.get('/api/hello', controller.hello);

    app.get('/api/genres/:id',...middleware,controller.getListOfBooksByGenre);
    app.get('/api/genres',...middleware, controller.getListOfAllGenres)
    app.post('/api/genres/search',...middleware, controller.getBooksByGenreName)
    app.post('/api/genres/create',...middleware, controller.createNewGenre)

    app.get('/api/book/:id',...middleware, controller.getSingleBook);
    app.get('/api/books',...middleware, controller.getListOfAllBooks);
    app.post('/api/books/search',...middleware, controller.getBooksByName)
    app.post('/api/books/create',...middleware, controller.createNewBook)
}

