const authController = require('../controllers/authController');

module.exports = (app) => {
    app.post('/api/login', authController.login);
    app.post('/api/logout',authController.logout)
    app.post('/api/register', authController.register);
};
