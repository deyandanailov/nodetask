const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    username: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        required: true,
        unique: true,
        dropDups: true
    },
    password: {
        type: String,
        required: true,
    },
    cookie: {
        type: String,
        default: ''
    },
}, {timestamps: true});

const UserModel = mongoose.model("User", UserSchema);

module.exports = {UserSchema, UserModel};
