const mongoose = require('mongoose');

const BookSchema = new mongoose.Schema({
    name: {
        type: String,
        default: ''
    },
    author: {
        type: String,
        default: ''
    },
    genre: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Genre"
    }
}, {timestamps: true});

const BookModel = new mongoose.model('Book', BookSchema);

module.exports = {BookModel, BookSchema}
