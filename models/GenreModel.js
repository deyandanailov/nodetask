const mongoose = require('mongoose');

const GenreSchema = new mongoose.Schema({
    name: {
        type: String,
        default: '',
        unique: true,
    }
}, {timestamps: true});

const GenreModel = mongoose.model('Genre', GenreSchema);

module.exports = {GenreModel, GenreSchema};