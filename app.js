const express = require('express')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const indexRoute = require('./routes/indexRoute');
const authRoute = require('./routes/authRoute');

const {isLoggedInMiddleware} = require('./middlewares/isLoggedInMiddleware')

const app = express();

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());

indexRoute(app,[isLoggedInMiddleware]);
authRoute(app);

app.use(express.static(__dirname + '/public/'));

app.use((err, req, res, next) => {
    console.log('---------------')
    console.log(err)
    console.log('---------------')
    res.status(422).send({result: 'error', status: 422, message: err.message});
});

module.exports = app;