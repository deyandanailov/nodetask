function makeString(length = 15) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function removeDuplicates(myArr, prop) {
    if (!prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.indexOf(obj) === pos;
        });
    } else return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}

function randomNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
module.exports = {makeString, removeDuplicates, randomNum};