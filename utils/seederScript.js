const genres = ["action", "adventure", "classics", "comics", "mystery", "fantasy", "horror"]
const books = [
    {name: "In Search of Lost Time", author: "Marcel Proust"},
    {name: "Ulysses ", author: "James Joyce"},
    {name: "Don Quixote", author: "Miguel de Cervantes"},
    {name: "One Hundred Years of Solitude", author: "Gabriel Garcia Marquez"},
    {name: "Moby Dick", author: "Herman Melville"},
]
let {createGenre, getAllGenres, getGenre} = require('../queries/genreQueries');
let {createBook, getAllBooks} = require('../queries/bookQueries');
const fetch = require('node-fetch');

function createGenres(numberOfGenres) {
    return new Promise(async (resolve, reject) => {
        try {
            console.log('start creating genres')
            for (let i = 0; i < numberOfGenres; i++) {
                let genreName = genres[i];
                console.log('name: ', genreName);
                let url = 'http://localhost:5000/api/genres/create';
                let response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({name: genreName}),
                });
                let json = await response.json();
                console.log('create response: ', json.msg)
            }
            resolve()
        } catch (e) {
            reject(e)
        }
    })
}

function createBooks(numberOfBooks) {
    return new Promise(async (resolve, reject) => {
        try {
            console.log('start creating books')
            for (let i = 0; i < numberOfBooks; i++) {
                let book = books.pop();

                // we assume that book is from that genre and find the proper id
                let genreName = getRandomElement(genres);
                console.log('genre name: ', genreName)
                // todo error in getGenre
                book.genre = genreName
                let url = 'http://localhost:5000/api/books/create';
                let response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(book),
                });
                let json = await response.json();
                console.log('create response: ', json.msg)
            }
            resolve()
        } catch (e) {
            reject(e)
        }
    })
}

function randomNum(min, max, float = false) {
    let weight = 1
    if (!float && float !== 0) {
        float = 0
        let minDec = Math.ceil(min) - min;
        let maxDec = Math.ceil(max) - max;
        if (minDec || maxDec) {
            let minLen = min.toString().replace('0.', '').length;
            let maxLen = max.toString().replace('0.', '').length;
            float = Math.max(minLen, maxLen)
        }
    }
    for (let i = 0; i < float; i++) {
        weight *= 10;
    }
    return Number((Math.floor(Math.random() * (max * weight - min * weight + 1)) / weight + min).toFixed(float || 0));
}


function getRandomElement(array) {
    return array[randomNum(0, array.length - 1)]
}
async function fillLibrary() {
    await createGenres(7);
    await createBooks(5);
}

fillLibrary();