const config = require("../config/mainConfig")
const nodemailer = require('nodemailer');

async function sendVerificationMail(userId, email) {
    try {/*
        const mailOptions = {
            //todo
            from: 'Extensions World <no-reply@extensionsworld.com>',
            to: email,
            subject: 'Email Verification Link',
            html: `<div><h3>Thanks for registering at Extensions World</h3> 
                To verify your mail <b><a href="${config.server.url}/verify?verificationToken=${userId}" target="_blank">click here</a></b><br>
                If the link is broken, just copy this one: ${config.server.url}/verify?verificationToken=${userId}
                </div>`
        };
        const transporter = nodemailer.createTransport(config.nodemailer);

        let info = await transporter.sendMail(mailOptions);
        console.log('Message sent: %s', info.messageId);*/
        console.log('add mail verification');
    } catch (e) {
        console.log('mail error: ', e);
    }
}

async function sendLostPasswordMail(passwordToken, email) {
    try {
        const mailOptions = {
            from: 'Extensions World <no-reply@extensionsworld.com>',
            to: email,
            subject: 'Recover lost password',
            html: `<div><h3>Recover your lost password</h3> 
                To create a new password <b><a href="${config.server.url}/resetPassword?resetToken=${passwordToken}" target="_blank">click here</a></b><br>
                If the link is broken, just copy this one: ${config.server.url}/resetPassword?resetToken=${passwordToken}
                </div>`
        };
        const transporter = nodemailer.createTransport(config.nodemailer);

        let info = await transporter.sendMail(mailOptions);
        console.log('Message sent: %s', info.messageId);
    } catch (e) {
        console.log('mail error: ', e)
    }
}

async function sendContactForm({nickname, email, subject, category, message}) {
    try {
        const mailOptions = {
            from: 'Extensions World Contact Form <contact@extensionsworld.com>',
            to: 'vladislav.kostov432@gmail.com',
            subject: category + ' - ' + subject,
            html: `<div><h3>${subject}<small>(${category})</small></h3>
                From:${nickname} <${email}><br/> 
               ${message}</div>`
        };
        const transporter = nodemailer.createTransport(config.nodemailer);

        let info = await transporter.sendMail(mailOptions);
        console.log('Message sent: %s', info.messageId);
    } catch (e) {
        console.log('mail error: ', e);
    }
}

module.exports = {
    sendVerificationMail,
    sendLostPasswordMail,
    sendContactForm
}