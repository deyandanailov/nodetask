const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);

function hashString(string) {
    return bcrypt.hashSync(string, salt);
}

function compareStrings(real, hashed) {
    return bcrypt.compareSync(real, hashed)
}

module.exports = {hashString,compareStrings}