const {userExists, createUser, updateUser, getUser} = require('../queries/userQueries');
const {compareStrings, hashString} = require('../utils/encryptionUtils');
const {makeString} = require('../utils/functionsUtils');

const usernameRegex = /^[а-яА-Яa-zA-Z0-9_=+\- ()\[\]{}]{4,16}$/
module.exports = {

    async register(req, res, next) {
        try {
            let user;
            let message;
            let isError = true;
            let {email, username, password, repPassword} = req.body
            if (!username.match(usernameRegex)) message = "Invalid Username!"
            else if (password.length < 6) message = "Password Too Short!";
            else if (password.length > 64) message = "Password Too Long!";
            else if (password !== repPassword) message = "Passwords Don't Match!";
            else if (await userExists({email})) message = "Email already exists!"
            else {
                isError = false
                user = await createUser({email, username, password: hashString(password)});
            }
            if (isError) {
                console.log('err: ', isError)
                res.send({message})
            } else {
                let cookie = makeString();
                await updateUser({_id: user._id}, {cookie});
                res.cookie('userCookie', cookie, {maxAge: 999999999, httpOnly: true});
                res.send({message: `New User - ${username} created!`});
            }
        } catch (e) {
            console.log(e)
            next(e, req, res, next)
        }
    },
    async login(req, res, next) {
        try {
            let message = 'Login Successful!';
            let {email, password} = req.body;

            if (!await userExists({email})) message = "Wrong email!";
            else {
                let user = await getUser({email});

                if (!compareStrings(password, user.password)) message = "Wrong Password";
                else {
                    let cookie = makeString();
                    await updateUser({_id: user._id}, {cookie});
                    res.cookie('userCookie', cookie, {maxAge: 999999999, httpOnly: true});
                    res.cookie('afterLogin', '', {maxAge: 0, httpOnly: true});
                    message = "Login Successful!";
                }
            }
            res.send({message})
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async logout(req, res, next) {
        try {
            let {email} = req.body;
            let user = await getUser({email});

            if (user.cookie) {
                res.cookie('userCookie', '', {maxAge: 0, httpOnly: true});
                res.cookie('afterLogin', '', {maxAge: 0, httpOnly: true});
                return res.send({message: "Logout Successful"})
            }
        } catch (e) {
            return res.send({message:"Logout not Successful"})

        }
    }
};


