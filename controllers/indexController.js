let {createBook, getBookById, getAllBooks} = require('../queries/bookQueries');
let {createGenre, getGenre, getAllGenres} = require('../queries/genreQueries');

module.exports = {
    hello(req, res, next) {
        try {
            res.send({express: 'Hello From Express'})
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async getListOfAllGenres(req, res, next) {
        try {
            let genres= await getAllGenres();
            res.send({genres});
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async createNewGenre(req, res, next) {
        try {
            let data = req.body;
            await createGenre(data);
            res.send({msg: `${data.name} genre created`});
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async getSingleBook(req, res, next) {
        try {
            let bookId = req.params.id;
            let book = await getBookById(bookId);
            res.send({book});
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async getListOfAllBooks(req, res, next) {
        try {
            let books= await getAllBooks();
            res.send({books});
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async getListOfBooksByGenre(req, res, next) {
        try {
            let genreId = req.params.id;
            let books = await getAllBooks({genre: genreId});
            res.send({books});
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async getBooksByName(req, res, next) {
        try {
            let name = req.body.name;
            let books = await getAllBooks({name});
            res.send({books});
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async getBooksByGenreName(req, res, next) {
        try {
            let genreName = req.body.name;
            let genre = await getGenre({name: genreName})
            let books = await getAllBooks({genre: genre._id});
            res.send({books});
        } catch (e) {
            next(e, req, res, next)
        }
    },
    async createNewBook(req, res, next) {
        try {
            let data = req.body;
            let genre = await getGenre({name: data.genre});
            data.genre = genre._id;
            await createBook(data);
            res.send({msg: `${data.name} book added to library from ${data.author} with genre ${data.genre}`});
        } catch (e) {
            next(e, req, res, next)
        }
    },
};