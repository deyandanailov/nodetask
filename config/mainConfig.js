const port = process.env.PORT || 5000
let databaseName = 'library';

module.exports = {
    server: {
        port
    },
    mongo: {
        options: {
            useNewUrlParser: true,
            keepAlive: 1,
            useUnifiedTopology: true
        },
        databaseName,
        devUrl: `mongodb://localhost/`,
        URL: '',
    }
}