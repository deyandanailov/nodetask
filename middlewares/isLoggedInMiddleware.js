let config = require("../config/mainConfig");
let {userExists} = require('../queries/userQueries');
module.exports = {
    isLoggedInMiddleware: async (req, res, next) => {
        let {userCookie} = req.cookies;
        let exists = await userExists({cookie: userCookie});
        if (!exists) {
            res.clearCookie('userCookie');
            if (userCookie) res.send( {message: "Invalid session, please login"})
            else res.send({message: 'welcome'})
        } else {
            try {
                return next();
            } catch (err) {
                return res.status(401).send({
                    result: 'error',
                    message: err.message,
                });
            }
        }
    }
};
