const {UserModel} = require('../models/UserModel')

async function createUser(userData) {
    const newUser = new UserModel(userData)
    return newUser.save();
}

async function userExists(query = {}) {
    let exists = UserModel.exists(query)
    if (!exists) this.message = 'No such user in our database';
    return exists
}

async function getUser(query) {
    return UserModel.findOne(query);
}

async function updateUser(userData, update, options = {new: true}) {
    return UserModel.findOneAndUpdate(userData, update, options)
}

async function updateMultipleUsers(usersQuery, update, options = {new: true}) {
    return UserModel.updateMany(usersQuery, update, options)
}

async function deleteUser(userData, options = {}) {
    return UserModel.findByIdAndRemove(userData, {...options})
}

module.exports = {
    createUser,
    getUser,
    userExists,
    updateUser,
    updateMultipleUsers,
    deleteUser
}
