let {BookModel} = require('../models/BookModel');

async function createBook(bookData={}) {
    let newBook = new BookModel(bookData);
    return await newBook.save();
}

async function getBookById(id) {
    return BookModel.findById(id);
}

async function getAllBooks(query = {}) {
    return BookModel.find(query);
}

module.exports = {createBook, getBookById, getAllBooks}