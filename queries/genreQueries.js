let {GenreModel} = require('../models/GenreModel');

async function createGenre(data={}) {
    let newGenre = new GenreModel(data);
    return await newGenre.save();
}

async function getGenreById(id) {
    return GenreModel.findById(id);
}

async function getGenre(query) {
    return GenreModel.findOne(query);
}

async function getAllGenres(query = {}) {
    return GenreModel.find(query);
}

module.exports = {createGenre, getGenreById, getGenre, getAllGenres}