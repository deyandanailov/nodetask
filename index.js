const app = require('./app')
const {mongo, server} = require('./config/mainConfig')
const mongoose = require('mongoose');

app.listen(server.port, () => {

    if (process.env.NODE_ENV !== 'test') {
        let mongoUrl = `${process.env.NODE_ENV == "dev" ? mongo.devUrl : mongo.URL}${mongo.databaseName}`

        connectMongo();

        function connectMongo() {
            return mongoose.connect(mongoUrl, mongo.options, (err) => {
                if (err) {
                    setTimeout(() => {
                        connectMongo();
                    }, 4000);
                    console.error('Failed to connect to db retrying', err);
                } else {

                }
            });
        }

        console.log('Running on port ' + server.port);
    } else {
        console.log('Running Tests')
    }
});
